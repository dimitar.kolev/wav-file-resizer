﻿#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "SystemFunctions.h"

int main(int argc, char* argv[]) {
    set_settings_from_argc(argc, argv);
    int result_of_wav_file_task = run_wav_file_task();
    return result_of_wav_file_task;
}
