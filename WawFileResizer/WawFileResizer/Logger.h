#pragma once
#include <stdio.h>
#include <stdbool.h>


#define ERROR(msg)\
		if (!silent) { \
			printf("\n Functuon: %s at line: %d\n [ERROR]: %s\n", __func__, __LINE__, msg);\
		} \
		exit(1);

#define WARNING(msg)\
		if (!silent) { \
			printf("\n Functuon: %s at line: %d\n [WARNING]: %s\n", __func__, __LINE__, msg);\
		} \
		exit(1);

#define INFO(...) printf(__VA_ARGS__)


