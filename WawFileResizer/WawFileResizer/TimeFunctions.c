#include "TimeFunctions.h"
#include "GlobalVars.h"

const Time* create_time(size_t secs) {
    Time* time = (Time*)malloc(sizeof(Time));
    if (NULL == time) {
        ERROR("Memory allocation time");
        return time;
    }

    time->all_in_secs = secs;
    time->hours = secs / 3600;
    secs = secs % 3600;
    time->minutes = secs / 60;
    secs = secs % 60;
    time->seconds = secs;
    return time;
}

void print_time(const Time* time) {
    if (NULL == time) {
        ERROR("Memory allocation time");
        return time;
    }

    INFO("%02hu:%02hu:%02hu", time->hours, time->minutes, time->seconds);
}

size_t input_time() {
    Time time;
    INFO("Input time in format 'hh:mm:ss': ");
    scanf_s("%d:%d:%d", &time.hours, &time.minutes, &time.seconds);
    int totalSeconds = time.hours * 3600 + time.minutes * 60 + time.seconds;
    return totalSeconds;
}