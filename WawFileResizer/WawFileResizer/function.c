#include "functions.h"

const WavHeader* const read_header(const FILE* const file) {
    WavHeader*  header = (WavHeader*)malloc(sizeof(WavHeader));
    int position;
    if (NULL == file)
    {
        printf("ERR (function read_header): Memory allocation header");
        return header;
    }

    // Set seek in the start of file
    position = ftell(file);
    fseek(file, 0, SEEK_SET);
    // Read the WAV file header
    fread(header, sizeof(*header), 1, file);
    fseek(file, position, SEEK_SET);
    return header;
}

size_t calculate_duration(const WavHeader* const header) {
    if (header == NULL)
    {
        printf("ERR (function calculate_duration): Memory allocation header");
        return 0;
    }

    return ((*header).chunk_size - 44) / ((*header).sample_rate * (*header).num_channels * (*header).bits_per_sample / 8);
}

void print_duration_by_header(const WavHeader* const header) {
    int duration = calculate_duration(header);
    if (NULL == header)
    {
        printf("ERR (function calculate_duration): Memory allocation header");
        return;
    }

    printf("Duration of the WAV file: ");
    Time* time = create_time(duration);
    print_time(time);
    printf(" seconds\n");
    free(time);
}

void print_duration_by_file(const FILE* const file) {
    if (NULL == file)
    {
        printf("ERR (function print_duration_by_file): Memory allocation file");
        return;
    }

    WavHeader* header = read_header(file);
    print_duration_by_header(header);
    free(header);
}

void print_header(const WavHeader* const header) {
    if (NULL == header)
    {
        printf("ERR (function print_header): Memory allocation header");
        return;
    }

    printf("Chunk ID: %c%c%c%c\n", (*header).chunk_id[0], (*header).chunk_id[1], (*header).chunk_id[2], (*header).chunk_id[3]);
    printf("Chunk size: %d\n", (*header).chunk_size);
    printf("Format: %c%c%c%c\n", (*header).format[0], (*header).format[1], (*header).format[2], (*header).format[3]);
    printf("Subchunk 1 ID: %c%c%c%c\n", (*header).subchunk_1_ID[0], (*header).subchunk_1_ID[1], (*header).subchunk_1_ID[2], (*header).subchunk_1_ID[3]);
    printf("Subchunk 1 size: %d\n", (*header).subchunk_1_size);
    printf("Audio format: %hd\n", (*header).audio_format);
    printf("Number of channels: %hd\n", (*header).num_channels);
    printf("Sample rate: %d\n", (*header).sample_rate);
    printf("Byte rate: %d\n", (*header).byte_rate);
    printf("Block align: %hd\n", (*header).block_align);
    printf("Bits per sample: %hd\n", (*header).bits_per_sample);
    printf("Subchunk 2 ID: %c%c%c%c\n", (*header).subchunk_2_id[0], (*header).subchunk_2_id[1], (*header).subchunk_2_id[2], (*header).subchunk_2_id[3]);
    printf("Subchunk 2 size: %d\n", (*header).subchunk_2_size);
}