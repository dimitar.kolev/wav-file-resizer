#pragma once
#include <stdio.h>
#include <stdlib.h>

#include "Time.h"
#include "Logger.h"
#include "SystemFunctions.h"
#include "GlobalVars.h"

//------------------------------------------
// DESCRIPTION:
//	Convert total secs  to time obj
// PARAMETERS:
//	secs
// RETURN VALUE:
//	new pointer of time obj with hours, mins, secs
//-------------------------------------------
const Time* create_time(size_t secs);

//------------------------------------------
// DESCRIPTION:
//	Print time
// PARAMETERS:
//	pointer of time obj
// RETURN VALUE:
//	none
//-------------------------------------------
void print_time(const Time* time);


//------------------------------------------
// DESCRIPTION:
//	Input time via console
// PARAMETERS:
//	none
// RETURN VALUE:
//	total secs
//-------------------------------------------
size_t input_time();