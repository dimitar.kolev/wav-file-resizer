#pragma once
#include "WavHeader.h"

typedef struct {
    WavHeader* header;
    short* data;
    size_t duration;
} WavFile;
