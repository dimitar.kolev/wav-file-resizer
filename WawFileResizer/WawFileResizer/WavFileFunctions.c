﻿#include "WavFileFunctions.h"

const char* const read_audio_data(const FILE* const file, unsigned int Subchunk2Size) {
    short* data = NULL;
    if (NULL == file)
    {
        ERROR("Memory allocation header");
        return data;
    }

    data = (short*)malloc(Subchunk2Size);
    fread(data, Subchunk2Size, 1, file);
    return data;
}

bool is_extension_wav(const char* const file_path) {
    const char* const extension = ".wav";
    size_t file_path_len = strlen(file_path);
    size_t extension_len = strlen(extension);

    if (extension_len > file_path_len)
        return false;

    return strcmp(&file_path[file_path_len - extension_len], extension);
}

const WavFile* const read_wav_file(const char* const file_path)
{
    FILE* file;
    WavFile* wav_file = (WavFile*)malloc(sizeof(WavFile));
    fopen_s(&file, file_path, "rb");
    if (file == NULL)
    {
        ERROR("Opening file.\n");
        free(wav_file);
        wav_file = NULL;
        return wav_file;
    }

    wav_file->header = read_wav_header(file);
    wav_file->data = read_audio_data(file, wav_file->header->Subchunk2Size);
    wav_file->duration = calculate_duration(wav_file->header);
    fclose(file);
    if (!is_wav_file(wav_file->header)) {
        free_wav_file(wav_file);
        ERROR("Memory allocation header");
        wav_file = NULL;
    }

    return wav_file;
}

void free_wav_file(WavFile* wav_file) {
    if (NULL == wav_file)
    {
        ERROR("Memory allocation header");
        return;
    }

    free(wav_file->data);
    free(wav_file->header);
    free(wav_file);
}

void save_wav_as(const WavFile* const wav_file, const char* const file_path){
    FILE* file;
    if (NULL == wav_file)
    {
        ERROR("Memory allocation header");
        return;
    }

    if (NULL == file_path)
    {
        ERROR("Memory allocation header");
        return;
    }

    fopen_s(&file, file_path, "wb");
    if (NULL == file)
    {
        ERROR("Memory allocation header");
        return;
    }

    fwrite(wav_file->header, sizeof(*(wav_file->header)), 1, file);
    fwrite(wav_file->data, wav_file->header->Subchunk2Size, 1, file);
    fclose(file);
}


void edit_wav(WavFile* wav_file, size_t new_duration) {
    if (NULL == wav_file)
    {
        ERROR("Memory allocation header");
        return;
    }

    if (new_duration > wav_file->duration) {
        extend_wav(wav_file, new_duration);
    }
    else if (new_duration < wav_file->duration) {
        cut_wav(wav_file, new_duration);
    }
}

void extend_wav(WavFile* const wav_file, size_t new_duration) {
     short* new_data;
    if (NULL == wav_file)
    {
        ERROR("Memory allocation header");
        return;
    }
    
    size_t old_Subchunk2Size = wav_file->header->Subchunk2Size;
    size_t num_samples = old_Subchunk2Size / (wav_file->header->BitsPerSample / 8) / wav_file->header->NumChannels;
    size_t samples_to_add = new_duration * wav_file->header->SampleRate - num_samples;
    if (samples_to_add <= 0) {
        ERROR("File duration is already greater than or equal to %f seconds\n", new_duration);
        return;
    }

    wav_file->header->Subchunk2Size += samples_to_add * wav_file->header->NumChannels * (wav_file->header->BitsPerSample / 8);
    wav_file->header->ChunkSize += samples_to_add * wav_file->header->NumChannels * (wav_file->header->BitsPerSample / 8);
    wav_file->duration = new_duration;
    //wav_file->data = (short*)realloc(wav_file->data, wav_file->header->Subchunk2Size);

    new_data = (short*)malloc(wav_file->header->Subchunk2Size);
    if (NULL == new_data) {
        ERROR("Memory allocation");
    }

    old_Subchunk2Size /= sizeof(wav_file->data[0]);
    for (size_t i = 0; i < old_Subchunk2Size; i++) {
        new_data[i] = wav_file->data[i];
    }

    samples_to_add /= sizeof(wav_file->data[0]);
    for (size_t i = 0; i < samples_to_add; i++) {
        new_data[old_Subchunk2Size+i] = 0;
        // wav_file->data[old_Subchunk2Size + i] = 0;
    }

    free(wav_file->data);
    wav_file->data = new_data;
}

void cut_wav(WavFile* const wav_file, size_t new_duration) {
    short* new_data;
    if (NULL == wav_file)
    {
        ERROR("Memory allocation header");
        return;
    }

    if (new_duration > wav_file->duration) {
        ERROR("File duration is already greater than or equal to %f seconds\n", new_duration);
        return;
    }

    size_t new_samples = new_duration * wav_file->header->SampleRate;
    wav_file->header->Subchunk2Size = new_samples * wav_file->header->NumChannels * (wav_file->header->BitsPerSample / 8);
    wav_file->header->ChunkSize = new_samples * wav_file->header->NumChannels * (wav_file->header->BitsPerSample / 8);
    wav_file->duration = new_duration;
    // wav_file->data= (short*)realloc(wav_file->data, wav_file->header->Subchunk2Size);
    new_data = (short*)malloc(wav_file->header->Subchunk2Size);

    if (NULL == new_data) {
        ERROR("Memory allocation");
    }
    size_t end_position = wav_file->header->Subchunk2Size / sizeof(wav_file->data[0]);
    for (size_t i = 0; i < end_position; i++) {
        new_data[i] = wav_file->data[i];
    }

    free(wav_file->data);
    wav_file->data = new_data;
}
