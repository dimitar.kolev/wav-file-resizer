#pragma once
#include <stdio.h>
#include <stdlib.h>

#include "WavHeader.h"
#include "time.h"

const WavHeader* const read_header(const FILE* const file);
size_t calculate_duration(const WavHeader* const header);

void print_duration_by_header(const WavHeader* const header);
void print_duration_by_file(const FILE* const file);
void print_header(const WavHeader* const);
