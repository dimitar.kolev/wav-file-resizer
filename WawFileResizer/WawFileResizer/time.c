#include "time.h"

const Time* create_time(unsigned int secs) {
    Time* time = (Time*)malloc(sizeof(Time));
    if (NULL == time) {
        printf("ERR (function create_time): Memory allocation time");
        return time;
    }

    time->all_in_secs = secs;
    time->hours = secs / 3600;
    secs = secs % 3600;
    time->minutes = secs / 60;
    secs = secs % 60;
    time->seconds = secs;
    return time;
}

void print_time(const Time* time) {
    if (NULL == time) {
        printf("ERR (function create_time): Memory allocation time");
        return time;
    }
    
    printf("%02hu:%02hu:%02hu", time->hours, time->minutes, time->seconds);
}