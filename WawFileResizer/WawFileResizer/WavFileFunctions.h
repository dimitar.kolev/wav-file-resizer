#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "WavHeader.h"
#include "WavHeaderFunctions.h"
#include "WavFile.h"
#include "GlobalVars.h"
#include "Logger.h"

//------------------------------------------
// DESCRIPTION:
//	Read audio data from wav
// PARAMETERS:
//	pointer of File 
//	Subchunk2Size from heder of file
// RETURN VALUE:
//	short array with audio
//-------------------------------------------
const char* const read_audio_data(const FILE* const file, unsigned int Subchunk2Size);

//------------------------------------------
// DESCRIPTION:
//	Read wav file
// PARAMETERS:
//	Path of file as string
// RETURN VALUE:
//	NULL (if file not exist or it is damaged) 
//	or
//  pointer of new WavFile obj (if file exist and it has currect header)
//-------------------------------------------
const WavFile* const read_wav_file(const char* const file_path);

//------------------------------------------
// DESCRIPTION:
//	Checks if the file has an extension ".wav"
// PARAMETERS:
//	Path of file as string
// RETURN VALUE:
//	Bool (True/False)
//-------------------------------------------
bool is_extension_wav(const char* const file_path);

//------------------------------------------
// DESCRIPTION:
//	Save WavFile obj to file
// PARAMETERS:
//	pointer of WavFile obj
//	Path of file
// RETURN VALUE:
//	None
//-------------------------------------------
void save_wav_as(const WavFile* const wav_file, const char* const file_path);

//------------------------------------------
// DESCRIPTION:
//	Edit WavFile obj with new duration
// PARAMETERS:
//	pointer of WavFile obj for edit
//	new duration
// RETURN VALUE:
//	none, but WavFile obj from parameters has new duration
//-------------------------------------------
void edit_wav(WavFile* const wav_file, size_t new_duration);

//------------------------------------------
// DESCRIPTION:
//	Extend WavFile obj with new duration
// PARAMETERS:
//	pointer of WavFile obj for edit
//	new duration
// RETURN VALUE:
//	none, but WavFile obj from parameters has new duration
//-------------------------------------------
void extend_wav(WavFile* const wav_file, size_t duration);

//------------------------------------------
// DESCRIPTION:
//	Cut WavFile obj with new duration
// PARAMETERS:
//	pointer of WavFile obj for edit
//	new duration
// RETURN VALUE:
//	none, but WavFile obj from parameters has new duration
//-------------------------------------------
void cut_wav(WavFile* const wav_file, size_t new_duration);


//------------------------------------------
// DESCRIPTION:
//	Free WavFile obj (destructor)
// PARAMETERS:
//	pointer of WavFile obj for edit
// RETURN VALUE:
//	none, but WavFile obj is freed
//-------------------------------------------
void free_wav_file(WavFile* const wav_file);
