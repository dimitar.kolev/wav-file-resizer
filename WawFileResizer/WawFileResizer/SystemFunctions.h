﻿#include <stdio.h>
#include <stdlib.h>

#include "Logger.h"
#include "WavHeader.h"
#include "WavFile.h"
#include "WavFileFunctions.h"

//------------------------------------------
// DESCRIPTION:
//	Аppends "_modified.wav" to the end of file path
// PARAMETERS:
//	source
// RETURN VALUE:
//	new string with file path and "_modified.wav"
//-------------------------------------------
const char* const create_path_with_modifide_file_name(const char* const file_path);

//------------------------------------------
// DESCRIPTION:
//	Edit WavFile obj
// PARAMETERS:
//	Pointer of WavFile obj
// RETURN VALUE:
//	edited WavFile obj
//-------------------------------------------
void edit_function(WavFile* const wav_file);

//------------------------------------------
// DESCRIPTION:
//	Set program settings from command row
// PARAMETERS:
//	argc
//	argv[]
// RETURN VALUE:
//	none, but settings sets in global vars
//-------------------------------------------
void set_settings_from_argc(int argc, char* argv[]);

//------------------------------------------
// DESCRIPTION:
//	 Save data to new file that name ends with _modified.wav;
// PARAMETERS:
//	none
// RETURN VALUE:
//	none
//-------------------------------------------
void save_function(const WavFile* const wav_file, const char* const file_path);

//------------------------------------------
// DESCRIPTION:
//	WavFile task:
//		1. Input file path;
//		2. Print header data;
//		3. Calculate duration;
//		4. Input new duration;
//		5. Edit audio data duration from wav file;
//		6. Save data to new file that name ends with _modified.wav;
// PARAMETERS:
//	none
// RETURN VALUE:
//	-1 - if something is wrong
//	0 - if everything is correctly
//-------------------------------------------
int run_wav_file_task();

//------------------------------------------
// DESCRIPTION:
//	 Input file path 
// PARAMETERS:
//	none
// RETURN VALUE:
//	none
//-------------------------------------------
const char* const input_file_path();
