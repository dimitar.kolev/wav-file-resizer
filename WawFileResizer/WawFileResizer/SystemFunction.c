#include "SystemFunctions.h"
#include "GlobalVars.h"

bool silent = 0;
bool overwrite = 0;

const char* const create_path_with_modifide_file_name(const char* const file_name) {
    if (NULL == file_name) {
        ERROR("Memory allocation time");
        return NULL;
    }

    const char* const modified = "-modified.wav";
    const char* const extension = ".wav";
    size_t file_name_len = strlen(file_name), modified_len = strlen(modified), extension_len = strlen(extension);
    size_t new_file_name_len = file_name_len + modified_len - extension_len;
    char* const new_file_name = (char*)calloc(new_file_name_len + 1, sizeof(char));
    strcpy(new_file_name, file_name);
    strcpy(new_file_name + file_name_len - extension_len, modified);
    new_file_name[new_file_name_len] = 0;
    return new_file_name;
}

const char* const input_file_path() {
    char* file_path = (char*)calloc(FILENAME_MAX, sizeof(char));
    INFO("Input file path: ");
    //file_name = "..\..\simples\simple1-modified.wav";
    fgets(file_path, FILENAME_MAX - 1, stdin);
    size_t file_path_len = strlen(file_path);
    file_path[file_path_len - 1] = 0;

    return file_path;
}

void edit_function(WavFile* const wav_file) {
    if (NULL == wav_file) {
        return;
    }

    size_t new_duration;
    // input new duration and edit data
    new_duration = input_time();
    //INFO("Input new duration: ");
    //scanf_s("%zu", &new_duration);
    edit_wav(wav_file, new_duration);
    INFO("Duration after edit:\n");
    // print duratation after editing
    print_duration_by_header(wav_file->header);
}

void set_settings_from_argc(int argc, char* argv[]) {
    silent = 0;
    overwrite = 0;

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "s") == 0) {
            silent = 1;
        }
        else if (strcmp(argv[i], "o") == 0) {
            overwrite = 1;
        }
    }
}

void save_function(const WavFile* const wav_file, const char * const file_path) {
    if (NULL == file_path) {
        return;
    }

    if (overwrite) {
        save_wav_as(wav_file, file_path);
        INFO("File is overwrited corretly %s\n", file_path);
    }
    else {
        char* new_file_path = create_path_with_modifide_file_name(file_path);
        save_wav_as(wav_file, new_file_path);
        INFO("File is save corretly %s\n", new_file_path);
        free(new_file_path);
    }
}

int run_wav_file_task() {
    char* file_path = input_file_path();
    WavFile* wav_file = read_wav_file(file_path);
    if (NULL == wav_file) {
        free(file_path);
        return -1;
    }

    // print header and duration
    print_header(wav_file->header);
    print_duration_by_header(wav_file->header);

    edit_function(wav_file);
    save_function(wav_file, file_path);

    free_wav_file(wav_file);
    free(file_path);
    return 0;
}
