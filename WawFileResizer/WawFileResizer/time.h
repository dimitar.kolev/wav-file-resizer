#pragma once
#include <stdio.h>
#include <stdlib.h>

typedef struct {
    unsigned int hours;
    unsigned int minutes;
    unsigned int seconds;
    unsigned int all_in_secs;
} Time;
