#include "WavHeaderFunctions.h"
#include "GlobalVars.h"

const WavHeader* const read_wav_header(const FILE* const file) {
    WavHeader* header = (WavHeader*)malloc(sizeof(WavHeader));
    if (NULL == file)
    {
        ERROR("Memory allocation header");
        return header;
    }

    fread(header, sizeof(*header), 1, file);
    return header;
}

size_t calculate_duration(const WavHeader* const header) {
    if (header == NULL)
    {
        ERROR("Memory allocation header");
        return 0;
    }

    return (header->Subchunk2Size) / ((*header).SampleRate * (*header).NumChannels * (*header).BitsPerSample / 8);
}
    
bool is_wav_file(const WavHeader* const wav_heaer) {
    const char* const chunk_id = wav_heaer->ChunkID;
    const char* const format = wav_heaer->Format;
    const char* const subchunk1_id = wav_heaer->Subchunk1ID;
    const char* const subchunk2_id = wav_heaer->Subchunk2ID;
    
    if ((chunk_id[0] != 'R' || chunk_id[1]!='I' || chunk_id[2] != 'F' || chunk_id[3] != 'F') ||
        (format[0] != 'W' || format[1] != 'A' || format[2] != 'V' || format[3] != 'E') ||
        (subchunk1_id[0] != 'f' || subchunk1_id[1] != 'm' || subchunk1_id[2] != 't' || subchunk1_id[3] != ' ') ||
        (subchunk2_id[0] != 'd' || subchunk2_id[1] != 'a' || subchunk2_id[2] != 't' || subchunk2_id[3] != 'a')) {
       
        ERROR("Wrong file");
        return false;
    }

    return true;
}

void print_duration_by_field(size_t duration) {
    INFO("The length of the file content is: ");
    Time* time = create_time(duration);
    print_time(time);
    INFO("\n");
    free(time);
}

void print_duration_by_header(const WavHeader* const header) {
    size_t duration = calculate_duration(header);
    if (NULL == header)
    {
        INFO("Memory allocation header");
        return;
    }

    print_duration_by_field(duration);
}

void print_header(const WavHeader* const header) {
    if (NULL == header)
    {
        ERROR("Memory allocation header");
        return;
    }

    INFO("Print header from file: \n");
    INFO("    ChunkID: %c%c%c%c\n", header->ChunkID[0], header->ChunkID[1], header->ChunkID[2], header->ChunkID[3]);
    INFO("    ChunkSize: %u\n", header->ChunkSize);
    INFO("    Format: %c%c%c%c\n", header->Format[0], header->Format[1], header->Format[2], header->Format[3]);
    INFO("    Subchunk1ID: %c%c%c%c\n", header->Subchunk1ID[0], header->Subchunk1ID[1], header->Subchunk1ID[2], header->Subchunk1ID[3]);
    INFO("    Subchunk1Size: %u\n", header->Subchunk1Size);
    INFO("    AudioFormat: %u\n", header->AudioFormat);
    INFO("    NumChannels: %u\n", header->NumChannels);
    INFO("    SampleRate: %u\n", header->SampleRate);
    INFO("    ByteRate: %u\n", header->ByteRate);
    INFO("    BlockAlign: %u\n", header->BlockAlign);
    INFO("    BitsPerSample: %u\n", header->BitsPerSample);
    INFO("    Subchunk2ID: %c%c%c%c\n", header->Subchunk2ID[0], header->Subchunk2ID[1], header->Subchunk2ID[2], header->Subchunk2ID[3]);
    INFO("    Subchunk2Size: %u\n", header->Subchunk2Size);
    INFO("\n");
}
