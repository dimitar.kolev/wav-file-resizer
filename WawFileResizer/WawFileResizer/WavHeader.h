#pragma once
typedef struct  {
    char ChunkID[4]; // Chunk type "RIFF".
    unsigned int ChunkSize; // The size of the entire WAV file in bytes, without sizeof(ChunkID) and sizeof(ChunkSize)
    char Format[4]; // "WAVE"
    char Subchunk1ID[4]; // "ftm "
    unsigned int Subchunk1Size; // size of Subchunk 1
    unsigned short AudioFormat; // format of audio
    unsigned short NumChannels; // numbers of channels
    unsigned int SampleRate; // represents the number of samples recorded per second.
    unsigned int ByteRate; // alculated as SampleRate * NumChannels * BitsPerSample / 8.
    unsigned short BlockAlign; // bytes for each block recorded for each sample -  NumChannels * BitsPerSample / 8.
    unsigned short BitsPerSample; // bits used to encode each sample
    char Subchunk2ID[4]; // "data"
    unsigned int Subchunk2Size; // represents the actual audio data size in bytes.
} WavHeader;
