#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "WavHeader.h"
#include "time.h"
#include "TimeFunctions.h"
#include "GlobalVars.h"

//------------------------------------------
// DESCRIPTION:
//	Read header from wav file
// PARAMETERS:
//	pointer of File 
// RETURN VALUE:
//	pointer of new WavHeader
//-------------------------------------------
const WavHeader* const read_wav_header(const FILE* const file);

//------------------------------------------
// DESCRIPTION:
//	Calculate audio duration by wav header
// PARAMETERS:
//	pointer of WavHeader pointer 
// RETURN VALUE:
//	duration in secs
//-------------------------------------------
size_t calculate_duration(const WavHeader* const header);

//------------------------------------------
// DESCRIPTION:
//	Print audio duration by wav header
// PARAMETERS:
//	pointer of WavHeader obj 
// RETURN VALUE:
//	none, but if header has data, print duratation of audio
//-------------------------------------------
void print_duration_by_header(const WavHeader* const header);

//------------------------------------------
// DESCRIPTION:
//	Print wav header
// PARAMETERS:
//	pointer of WavHeader obj 
// RETURN VALUE:
//	none, but if header has data, print all data from WavHeader obj to console
//-------------------------------------------
void print_header(const WavHeader* const);

//------------------------------------------
// DESCRIPTION:
//	Check that the wav file has correct header data
// PARAMETERS:
//	pointer of WavHeader obj 
// RETURN VALUE:
//	bool (true/false)
//-------------------------------------------
bool is_wav_file(const WavHeader* const header);
